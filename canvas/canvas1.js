$(document).ready(function() {

    var canvases = {
        canvas1 : new fabric.Canvas('canvas1'),
        canvas2 : new fabric.Canvas('canvas2')
    };

    $('#addCanvas1Rect').click(function(){
        canvases.canvas1.add(new fabric.Rect({ left: 20, top: 20, fill: 'black', stroke: 'red', strokeWidth: 3, width: 100, height: 100 }));
    });

    $('#addCanvas2Rect').click(function(){
        canvases.canvas2.add(new fabric.Rect({ left: 20, top: 20, fill: 'black', stroke: 'red', strokeWidth: 3, width: 100, height: 100 }));
    });

    $('#addCanvas1Circle').click(function(){
        canvases.canvas1.add(new fabric.Circle({radius: 50, left: 70, top: 250, fill: 'black', stroke: 'red', strokeWidth: 3, originX: 'center', originY: 'center' }));
    });

    $('#addCanvas2Circle').click(function(){
        canvases.canvas2.add(new fabric.Circle({radius: 50, left: 70, top: 250, fill: 'black', stroke: 'red', strokeWidth: 3, originX: 'center', originY: 'center' }));
    });

    $('#clearCanvas1').click(function(){
        canvases.canvas1.clear();
    });

    $('#clearCanvas2').click(function(){
        canvases.canvas2.clear();
    });
    
    var selectedObject, fromCanvas;
	pressed = false;;

    canvases.canvas1.on('mouse:down', function() {
        if(this.getActiveObject()) {
			pressed = true;
            selectedObject  = $.extend({}, this.getActiveObject());
            fromCanvas = this.lowerCanvasEl.id;
        }
    });

    canvases.canvas2.on('mouse:down', function() {
        if(this.getActiveObject()) {
			pressed = true;
            selectedObject  = $.extend({}, this.getActiveObject());
            fromCanvas = this.lowerCanvasEl.id;
        }
    });
    
	$(document).on('mousemove', function(evt) {
		if(pressed) {
			if(evt.target.localName === 'canvas' && fromCanvas) {
            canvasId = $(evt.target).siblings().attr('id');
            if(canvasId !== fromCanvas) {
                var location = getDropLocation(canvases[canvasId].lowerCanvasEl, evt);
                selectedObject.set({left: location.x });
                selectedObject.set({top: location.y });
				canvases[canvasId].setActiveObject(selectedObject);
            }
        }  
		}		
    }); 
	
    $(document).on('mouseup', function(evt) {
		if(pressed) {
		pressed = false;
        if(evt.target.localName === 'canvas' && fromCanvas) {
            canvasId = $(evt.target).siblings().attr('id');
            if(canvasId !== fromCanvas) {
                var location = getDropLocation(canvases[canvasId].lowerCanvasEl, evt);
                selectedObject.set({left: location.x });
                selectedObject.set({top: location.y });
                canvases[canvasId].add(selectedObject);
                canvases[canvasId].renderAll();
            }
        } 
		}		
    }); 

    function getDropLocation(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
      }
    
});
