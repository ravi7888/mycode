var data;

function getJson() {
	var xmlHttpRequest = new XMLHttpRequest();
	xmlHttpRequest.open('GET', 'https://jsonplaceholder.typicode.com/posts', true);
	xmlHttpRequest.responseType = 'json';
	xmlHttpRequest.onload = function() {
		var status = xmlHttpRequest.status;
		if (status == 200) {
			data = xmlHttpRequest.response;
			select = document.getElementById('userid');
			var opt = document.createElement('option');
			opt.value = '';	opt.innerHTML = '';	select.appendChild(opt);
			for(i=0; i<data.length; i++) {
				var opt = document.createElement('option');
				opt.value = data[i].id;
				opt.innerHTML = data[i].id;
				select.appendChild(opt);
			}
		} else {
			alert("Failed to fetch JSON");
		}
	};
	xmlHttpRequest.send();
};

getJson();

function displayElementsOnID(){
	var val = document.getElementById('userid').value;
	showHideElements();
	titleSelect = document.getElementById('title1');
	titleSelect.innerHTML = '';
	for(i=0; i<data.length; i++) {
			var opt = document.createElement('option');
			opt.value = data[i].title;
			opt.innerHTML = data[i].title;
			if(val == data[i].id) {
				opt.selected = true;
				document.getElementById('tarea1').innerHTML = data[i].body;
			}
			titleSelect.appendChild(opt);
		}
}

function displayElementsOnTitle(){
	var val = document.getElementById('title1').value;
	idSelect = document.getElementById('userid');
	idSelect.innerHTML = '';
	for(i=0; i<data.length; i++) {
			var opt = document.createElement('option');
			opt.value = data[i].id;
			opt.innerHTML = data[i].id;
			if(val == data[i].title) {
				opt.selected = true;
				document.getElementById('tarea1').innerHTML = data[i].body;
			}
			idSelect.appendChild(opt);
		}
		showHideElements();
}

function showHideElements() {
	var val = document.getElementById('userid').value;
	if(val == '') {
		document.getElementById('title').style.display = "none";
		document.getElementById('tarea').style.display = "none";
	
	} else if(val%2 == 0) {
		document.getElementById('title').style.display = "block";
		document.getElementById('tarea').style.display = "block";
	} else {
		document.getElementById('title').style.display = "block";
		document.getElementById('tarea').style.display = "none";
	}
}